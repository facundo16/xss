import Head from 'next/head'
import { useRouter } from 'next/router'

import styles from '../styles/Home.module.css'

export default function Home() {
  const router = useRouter();

  return (
    <div className={styles.container}>
      <Head>
        <title>Meli Xss Chat</title>
        <meta name="description" content="Vulnerable app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Meli Chat 💬
        </h1>
        <form className="col-lg-4 col-md-6 col-sm-8 col-12">
          <div className="mb-3">
            <input type="text" placeholder="User" className="form-control" id="user" />
          </div>
          <div className="mb-3">
            <input type="password" placeholder="Pass" className="form-control" id="pass" />
          </div>
          <a href="/sign-up">Do you need a user? Create account</a>
          <button type="submit" className="btn btn-primary col-12">Login</button>
        </form>
      </main>
    </div>
  )
}
